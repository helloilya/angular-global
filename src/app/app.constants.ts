export const constants = {
  searchBarOrderOptionList: {
    date: 'Date',
    duration: 'Duration',
  },
  courseBorderColorList: {
    green: 'brdr--green',
    blue: 'brdr--blue',
    gray: 'brdr--off-white',
  }
};