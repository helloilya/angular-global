import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

import { constants } from './../app.constants';

@Directive({
  selector: '[courseBorder]',
})

export class CourseBorderDirective implements OnInit {

  private currentDate = new Date().getTime();
  private shiftDays = 86400000 * 14;

  @Input('courseBorder') courseDate: string;

  constructor(private renderer: Renderer2,
              private element: ElementRef) {}

  public ngOnInit() {
    this.setBorderColor();
  }

  private setBorderColor() {
    const freshDate = this.currentDate - this.shiftDays;
    const courseDate = Number(this.courseDate);
    let courseClassName = constants.courseBorderColorList.gray;

    if(courseDate < this.currentDate && courseDate >= freshDate) {
      courseClassName = constants.courseBorderColorList.green;
    } else if(courseDate > this.currentDate) {
      courseClassName = constants.courseBorderColorList.blue;
    }

    Object.keys(constants.courseBorderColorList).forEach((classKey) => {
      this.renderer.removeClass(this.element.nativeElement, classKey);
    });
    this.renderer.addClass(this.element.nativeElement, courseClassName);
  }

}