export interface SearchBarModel {
  search?: string;
  order?: string;
  outdated?: boolean;
}