import { AuthorModel } from './author.model';

export interface CourseModel {
  key: string;
  name: string;
  description: string;
  authors: AuthorModel[];
  date: number;
  duration: number;
  favorite: boolean;
}