import { AuthorModel } from './author.model';

export interface AuthorControlModel extends AuthorModel {
  selected: boolean;
}