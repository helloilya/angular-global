export interface BreadcrumbModel {
  name: string;
  url: string;
}