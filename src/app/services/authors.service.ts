import { AngularFirestore } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

import { AuthorModel } from '../models/author.model';

@Injectable()

export class AuthorsService {

  constructor(private afs: AngularFirestore) {}

  public get(): Observable<AuthorModel[]> {
    return this.afs.collection('authors').valueChanges().map((response: AuthorModel[]) => response);
  }

}