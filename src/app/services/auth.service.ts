import { AngularFirestore, DocumentChangeAction } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/take';

import { NavigationService } from '../services/navigation.service';
import { AuthModel } from '../models/auth.model';

@Injectable()

export class AuthService {

  private tokenKeyName: string = 'token';
  private subject$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private afs: AngularFirestore,
              private navigationService: NavigationService) {}

  isAuth(): boolean {
    return !!localStorage.getItem(this.tokenKeyName);
  }

  login(login: string, password: string): Observable<boolean> {
    return this.afs.collection('users', (reference) => reference.where('login', '==', login)).snapshotChanges().take(1).map((changeActions: DocumentChangeAction[]) => {
      let response = false;
      changeActions.forEach((action) => {
        if(action.payload.doc.data().password === password) {
          localStorage.setItem(this.tokenKeyName, action.payload.doc.id);
          this.navigationService.navigate('courses');
          this.subject$.next(true);
          response = true;
        }
      });
      return response;
    });
  }

  logout() {
    localStorage.removeItem(this.tokenKeyName);
    this.navigationService.navigate('');
    this.subject$.next(false);
  }

  authStatus(): Observable<boolean> {
    this.subject$.next(this.isAuth());
    return this.subject$.asObservable();
  }

}