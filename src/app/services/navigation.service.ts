import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()

export class NavigationService {

  constructor(private route: ActivatedRoute,
              private router: Router) {}

  public navigate(routeName: string) {
    this.router.navigate([routeName]);
  }

  public navigateWithParams(routeName: string, queryParams: object) {
    Object.keys(queryParams).forEach((key) => {
      if(!queryParams[key]) {
        delete queryParams[key];
      }
    });

    this.router.navigate([routeName], {
      queryParams: queryParams,
    });
  }

}