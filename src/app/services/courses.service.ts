import { AngularFirestore, DocumentChangeAction } from 'angularfire2/firestore';
import { DocumentSnapshot } from 'firebase/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

import { CourseModel } from '../models/course.model';

@Injectable()

export class CoursesService {

  constructor(private afs: AngularFirestore) {}

  public get(): Observable<CourseModel[]> {
    return this.afs.collection('courses').snapshotChanges().map((changeActions: DocumentChangeAction[]) => {
      return changeActions.map((action) => {
        const data = action.payload.doc.data() as CourseModel;
              data.key = action.payload.doc.id;
        return data;
      });
    });
  }

  public getItem(key: string): Observable<CourseModel> {
    return this.afs.collection('courses').doc(key).snapshotChanges().map((snapshot: DocumentSnapshot) => {
      const data = snapshot.payload.data() as CourseModel;
            data.key = snapshot.payload.id;
      return data;
    });
  }

  public create() {
    return this.afs.collection('courses').add({
      name: 'New course',
      description: '',
      authors: [],
      date: new Date().getTime(),
      duration: 0,
      favorite: false,
    });
  }

  public update(course: CourseModel) {
    const data = Object.assign({}, course);
    delete data.key;
    return this.afs.collection('courses').doc(course.key).update(data);
  }

  public delete(key: string) {
    this.afs.collection('courses').doc(key).delete();
  }

}