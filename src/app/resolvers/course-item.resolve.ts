import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import 'rxjs/add/operator/take';

import { CourseModel } from '../models/course.model';
import { CoursesService } from '../services/courses.service';

@Injectable()

export class CourseItemResolve implements Resolve<CourseModel> {

  constructor(private coursesService: CoursesService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.coursesService.getItem(route.paramMap.get('key')).take(1);
  }

}