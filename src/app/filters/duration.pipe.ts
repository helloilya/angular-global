import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})

export class DurationPipe implements PipeTransform {

  transform(duration: number) {
    const minutes = duration % 60;
    const hours = Math.floor(duration / 60);

    let result = '';
    if(hours) {
      result += `${hours}h `;
    }
    if(minutes) {
      result += `${minutes}min`;
    }
    return result.trim();
  }

}