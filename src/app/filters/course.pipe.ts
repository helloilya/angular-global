import { Pipe, PipeTransform } from '@angular/core';

import { CourseModel } from '../models/course.model';
import { SearchBarModel } from '../models/search-bar.model';

@Pipe({
  name: 'courseFilter'
})

export class CourseFilterPipe implements PipeTransform {

  private currentDate = new Date().getTime();
  private shiftDays = 86400000 * 14;

  transform(list: CourseModel[], args: SearchBarModel) {
    if(!list) {
      return list;
    }

    if(args && args.order) {
      list.sort((a: CourseModel, b: CourseModel): number => a[args.order] - b[args.order]);
    }
    if(args && args.search) {
      list = list.filter((item) => item.name.toLowerCase().indexOf(args.search.toLowerCase()) >= 0);
    }
    if(args && args.outdated) {
      list = list.filter((item) => item.date > this.currentDate - this.shiftDays);
    }
    return list;
  }

}