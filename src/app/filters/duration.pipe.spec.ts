import { DurationPipe } from './duration.pipe';

describe('DurationPipe', () => {

  let pipe: DurationPipe;

  beforeEach(() => {
    pipe = new DurationPipe();
  });

  it('should return hours diration', () => {
    const value = 60;

    expect(pipe.transform(value)).toBe('1h');
  });

  it('should return full diration format', () => {
    const value = 135;

    expect(pipe.transform(value)).toBe('2h 15min');
  });

});