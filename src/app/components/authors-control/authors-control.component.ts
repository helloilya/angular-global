import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { AuthorModel } from '../../models/author.model';
import { AuthorControlModel } from '../../models/author-control.model';
import { AuthorsService } from '../../services/authors.service';

@Component({
  selector: 'authors-control-component',
  templateUrl: './authors-control.component.html',
  styleUrls: ['./authors-control.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => AuthorsControlComponent),
    multi: true,
  }],
})

export class AuthorsControlComponent implements ControlValueAccessor, OnInit {

  @Input() classCondition: boolean;

  private authorList: AuthorControlModel[];
  private selectedAuthorList: AuthorModel[] = [];
  private propagateChange = (_: any) => {};

  constructor(private authorsService: AuthorsService,
              private cdRef: ChangeDetectorRef) {}

  public ngOnInit() {
    this.authorsService.get().map((list: AuthorModel[]) => {
      this.authorList = list.map((item) => ({
        selected: this.selectedAuthorList.some((author) => author.mail === item.mail),
        name: item.name,
        surname: item.surname,
        mail: item.mail,
      }));
      this.cdRef.markForCheck();
    }).take(1).subscribe();
  }

  public writeValue(authors: AuthorModel[]) {
    if(authors) {
      this.selectedAuthorList = authors;
    }
  }

  public registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  public registerOnTouched(fn: any) {}

  public change(event) {
    this.selectedAuthorList = [];
    this.authorList.forEach((author) => author.selected = false);
    if(event.target.selectedOptions) {
      Array.from(event.target.selectedOptions).forEach((element: HTMLOptionElement) => {
        let selectedAuthor;
        this.authorList.map((author) => {
          if(author.mail === element.value) {
            author.selected = true;
            selectedAuthor = Object.assign({}, author);
            delete selectedAuthor.selected;
          }
        });
        this.selectedAuthorList.push(selectedAuthor);
      });
    }
    this.propagateChange(this.selectedAuthorList);
  }
}