import { Component, EventEmitter, OnInit, Output, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { NavigationService } from '../../services/navigation.service';
import { SearchBarModel } from './../../models/search-bar.model';
import { constants } from './../../app.constants';

@Component({
  selector: 'search-bar-component',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SearchBarComponent implements OnInit {

  @Output() onUpdated = new EventEmitter<SearchBarModel>();

  public objectKeys = Object.keys;
  public orderOptionList = constants.searchBarOrderOptionList;
  public model: SearchBarModel = {};
  private subscription: Subscription;

  constructor(private navigationService: NavigationService,
              private route: ActivatedRoute) {}

  public ngOnInit() {
    this.subscription = this.route.queryParams.subscribe((params) => {
      this.model = { ...params as SearchBarModel };
      if(!this.model.order) {
        this.model.order = this.objectKeys(this.orderOptionList)[0];
      }
      this.onUpdated.emit(this.model);
    });
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public filter() {
    this.navigationService.navigateWithParams('courses', this.model);
  }

}