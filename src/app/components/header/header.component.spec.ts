import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of';

import { AuthService } from '../../services/auth.service';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {

  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  const authServiceMock = {
    authStatus: (): Observable<boolean> => {
      return Observable.of(true);
    },
    logout: () => {},
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [{
        provide: AuthService,
        useValue: authServiceMock,
      }],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
  });

  it('should set valid isAuth value', () => {
    spyOn(authServiceMock, 'authStatus');

    component.ngOnInit();

    expect(component.isAuth).toBe(true);
  });

  it('should call authService.logout', () => {
    spyOn(authServiceMock, 'logout');

    component.logout();

    expect(authServiceMock.logout).not.toHaveBeenCalled();
  });

});