import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'header-component',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})

export class HeaderComponent implements OnInit {

  public isAuth = false;
  private subscription: Subscription;

  constructor(private authService: AuthService) {}

  public ngOnInit() {
    this.subscription = this.authService.authStatus().subscribe((value) => {
      this.isAuth = value;
    });
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public logout() {
    this.authService.logout();
  }

}