import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

import { CoursesService } from '../../services/courses.service';
import { CourseModel } from '../../models/course.model';
import { messages } from './../../app.messages';

@Component({
  selector: 'course-card-component',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class CourseCardComponent implements OnInit {

  @Input() course: CourseModel;

  constructor(private coursesService: CoursesService) {}

  public ngOnInit() {}

  public deleteCourse(key: string) {
    if (window.confirm(messages.courses.deleteWindow)) {
      this.coursesService.delete(key);
    }
  }

}