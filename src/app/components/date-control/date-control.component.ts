import { Component, forwardRef, Input, Renderer2, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'date-control-component',
  templateUrl: './date-control.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DateControlComponent),
    multi: true,
  }],
})

export class DateControlComponent implements ControlValueAccessor {

  @ViewChild('input') input;
  @Input() classCondition: boolean;

  private propagateChange = (_: any) => {};

  constructor(private renderer: Renderer2) {}

  public writeValue(value: number) {
    if(value) {
      const date = new DatePipe('en-US').transform(value, 'yyyy-MM-dd');
      this.renderer.setProperty(this.input.nativeElement, 'value', date);
    }
  }

  public registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  public registerOnTouched(fn: any) {}

  public change(value: string) {
    const date = this.dateFormation(value);
    this.propagateChange(date);
  }

  private dateFormation(date: string): number {
    if(date) {
      const splitDate = date.split('-');
      return new Date(splitDate[1] + '/' + splitDate[2] + '/' + splitDate[0]).getTime();
    }
    return undefined;
  }

}