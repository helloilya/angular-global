import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, PRIMARY_OUTLET } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { BreadcrumbModel } from '../../models/breadcrumb.model';

@Component({
  selector: 'breadcrumbs-component',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class BreadcrumbsComponent implements OnInit {

  public breadcrumbList$: Observable<BreadcrumbModel[]>;

  constructor(private route: ActivatedRoute,
              private router: Router) {}

  public ngOnInit() {
    this.breadcrumbList$ = this.router.events.filter((event) => event instanceof NavigationEnd).map(() => this.getBreadcrumbs(this.route.root));
  }

  private getBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbList: BreadcrumbModel[] = []): BreadcrumbModel[] {
    const name = route.routeConfig && route.routeConfig.data && route.routeConfig.data['breadcrumb'];
    let path = route.routeConfig && route.routeConfig.path;
    route.snapshot.paramMap.keys.forEach((key) => {
      path = path.replace(`:${key}`, route.snapshot.paramMap.get(key));
    });

    if(url) {
      path = `${url}/${path}`;
    }
    if(name && path) {
      breadcrumbList.push({
        name: name,
        url: path,
      });
    }

    if(route.firstChild) {
      this.getBreadcrumbs(route.firstChild, path, breadcrumbList);
    }
    return breadcrumbList;
  }

}