import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './../../guard/auth.guard';
import { AppLoginComponent } from './login.component';

const routes: Routes = [
  {
    path: '',
    component: AppLoginComponent,
    canActivate: [
      AuthGuard,
    ],
  }
];

@NgModule({
  declarations: [
    AppLoginComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    AuthGuard,
  ]
})

export class AppLoginModule {}