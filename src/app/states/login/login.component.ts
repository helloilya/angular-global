import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { AuthModel } from '../../models/auth.model';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class AppLoginComponent implements OnInit {

  private isInvalidPair$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private authService: AuthService) {}

  public ngOnInit() {}

  public login(form: AuthModel) {
    this.authService.login(form.login, form.password).subscribe((response) => {
      this.isInvalidPair$.next(!response);
    });
  }

}