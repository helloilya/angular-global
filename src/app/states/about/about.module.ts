import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppAboutComponent } from './about.component';

const routes: Routes = [
  {
    path: '',
    component: AppAboutComponent,
  }
];

@NgModule({
  declarations: [
    AppAboutComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})

export class AppAboutModule {}