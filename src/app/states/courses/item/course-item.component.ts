import { LOCALE_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { CourseModel } from '../../../models/course.model';
import { CoursesService } from '../../../services/courses.service';
import { NavigationService } from '../../../services/navigation.service';

@Component({
  selector: 'app-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class AppCourseItemComponent implements OnInit {

  public course: CourseModel;

  constructor(private coursesService: CoursesService,
              private navigationService: NavigationService,
              private route: ActivatedRoute) {}

  public ngOnInit() {
    this.course = this.route.snapshot.data['course'];
  }

  public confirm() {
    this.coursesService.update(this.course).then(() => {
      this.navigationService.navigate('courses');
    });
  }

}