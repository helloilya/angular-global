import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppCourseItemComponent } from './course-item.component';
import { AuthorsControlComponent } from '../../../components/authors-control/authors-control.component';
import { CourseItemResolve } from './../../../resolvers/course-item.resolve';
import { CoursesGuard } from './../../../guard/courses.guard';
import { CoursesSharedModule } from './../courses-shared.module';
import { DateControlComponent } from '../../../components/date-control/date-control.component';
import { DateValidator } from './../../../validators/date.validator';
import { NumberValidator } from './../../../validators/number.validator';

const routes: Routes = [
  {
    path: '',
    component: AppCourseItemComponent,
    resolve: {
      course: CourseItemResolve,
    },
    canActivate: [
      CoursesGuard,
    ],
  }
];

@NgModule({
  declarations: [
    AppCourseItemComponent,
    AuthorsControlComponent,
    DateControlComponent,
    DateValidator,
    NumberValidator,
  ],
  imports: [
    CommonModule,
    CoursesSharedModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    CoursesGuard,
    CourseItemResolve,
  ]
})

export class AppCourseItemModule {}