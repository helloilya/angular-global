import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { CourseModel } from '../../../models/course.model';
import { CoursesService } from '../../../services/courses.service';
import { NavigationService } from '../../../services/navigation.service';
import { SearchBarModel } from './../../../models/search-bar.model';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class AppCourseListComponent implements OnInit {

  public courseFilterQuery;
  public courseList$: Observable<CourseModel[]>;

  constructor(private coursesService: CoursesService,
              private navigationService: NavigationService) {}

  public ngOnInit() {
    this.courseList$ = this.coursesService.get();
  }

  public updateFilter(query: SearchBarModel) {
    this.courseFilterQuery = query;
  }

  public createCourse() {
    this.coursesService.create().then((response) => {
      this.navigationService.navigate('courses/item/' + response.id);
    });
  }

}