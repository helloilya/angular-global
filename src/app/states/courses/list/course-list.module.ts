import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppCourseListComponent } from './course-list.component';
import { CourseCardComponent } from './../../../components/course-card/course-card.component';
import { CourseBorderDirective } from './../../../directives/course-border.directive';
import { CourseFilterPipe } from './../../../filters/course.pipe';
import { CoursesGuard } from './../../../guard/courses.guard';
import { CoursesSharedModule } from './../courses-shared.module';
import { SearchBarComponent } from './../../../components/search-bar/search-bar.component';

const routes: Routes = [
  {
    path: '',
    component: AppCourseListComponent,
    canActivate: [
      CoursesGuard,
    ],
  }
];

@NgModule({
  declarations: [
    AppCourseListComponent,
    CourseFilterPipe,
    CourseBorderDirective,
    CourseCardComponent,
    SearchBarComponent,
  ],
  imports: [
    CommonModule,
    CoursesSharedModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    CoursesGuard,
  ]
})

export class AppCourseListModule {}