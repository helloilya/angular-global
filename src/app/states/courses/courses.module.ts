import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './list/course-list.module#AppCourseListModule',
  },
  {
    path: 'item/:key',
    loadChildren: './item/course-item.module#AppCourseItemModule',
    data: {
      breadcrumb: 'Edit course',
    },
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})

export class AppCoursesModule {}