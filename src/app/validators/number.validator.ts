import { Directive } from '@angular/core';
import { NG_VALIDATORS, FormControl, ValidatorFn, Validator } from '@angular/forms';

@Directive({
  selector: '[numberValidator][ngModel]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: NumberValidator,
    multi: true,
  }],
})

export class NumberValidator implements Validator {

  public validator: ValidatorFn;

  constructor() {
    this.validator = this.validateNumber();
  }

  public validate(control: FormControl) {
    return this.validator(control);
  }

  private validateNumber(): ValidatorFn {
    return (control: FormControl) => {
      const isValid = /^[\d]+$/g.test(control.value);
      return isValid ? null : {
        numberValidator: false,
      };
    };
  }

}