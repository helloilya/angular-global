import { Directive } from '@angular/core';
import { NG_VALIDATORS, FormControl, ValidatorFn, Validator } from '@angular/forms';

@Directive({
  selector: '[dateValidator][ngModel]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: DateValidator,
    multi: true,
  }],
})

export class DateValidator implements Validator {

  public validator: ValidatorFn;

  constructor() {
    this.validator = this.validateDate();
  }

  public validate(control: FormControl) {
    return this.validator(control);
  }

  private validateDate(): ValidatorFn {
    return (control: FormControl) => {
      const isValid = /^[\d]{13}$/g.test(control.value);
      return isValid ? null : {
        dateValidator: false,
      };
    };
  }

}