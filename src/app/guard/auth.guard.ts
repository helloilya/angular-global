import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { NavigationService } from '../services/navigation.service';

@Injectable()

export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService,
              private navigationService: NavigationService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if(this.authService.isAuth() && state.url === '/') {
      this.navigationService.navigate('courses');
    } else {
      return true;
    }
  }

}