import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { environment } from './../environments/environment';

import { AppComponent } from './app.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

import { AuthService } from './services/auth.service';
import { AuthorsService } from './services/authors.service';
import { CoursesService } from './services/courses.service';
import { NavigationService } from './services/navigation.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: './states/login/login.module#AppLoginModule',
  },
  {
    path: 'about',
    loadChildren: './states/about/about.module#AppAboutModule',
    data: {
      breadcrumb: 'About',
    },
  },
  {
    path: 'courses',
    loadChildren: './states/courses/courses.module#AppCoursesModule',
    data: {
      breadcrumb: 'Courses',
    },
  },
  {
    path: '**',
    redirectTo: '',
  }
];

@NgModule({
  declarations: [
    AppComponent,
    BreadcrumbsComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    RouterModule.forRoot(routes, { useHash: true }),
  ],
  providers: [
    AuthService,
    AuthorsService,
    CoursesService,
    NavigationService,
  ],
  bootstrap: [
    AppComponent,
  ]
})

export class AppModule {}